import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
// import Home from '../views/Home.vue'
import TimeLine from '../views/Timeline.vue'
import Profile from '../views/Profile.vue'
import Users from '../views/Users.vue'
import { authGuard } from '../auth/authGuard'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: TimeLine
  },
  {
    path: '/profile/:userName',
    name: 'Profile',
    component: Profile,
    beforeEnter: authGuard
  },
  {
    path: '/profile',
    name: 'ProfileCurrentUser',
    component: Profile,
    beforeEnter: authGuard
  },
  {
    path: '/users',
    name: 'Users',
    component: Users,
    beforeEnter: authGuard
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
