import { getInstance } from '@/auth'
import { VueAuth } from '../auth/VueAuth'

export default class APIService {
  private baseUrl = 'http://localhost:8083/'
  private _auth!: VueAuth

    get auth () {
      if (this._auth) {
          return this._auth
      }
      this._auth = getInstance()
      return this._auth
    }

    get token () {
      if (this.auth.isAuthenticated) {
          return (async () => {
              await this.waitForAuthLoading()
              return this.auth.getTokenSilently({})
          })()
      }
      return ''
    }

    async waitForAuthLoading () {
      return new Promise<void>((response) => {
          if (!this.auth.loading) {
              return response()
          }
          const unwatch: any = this.auth.$watch('loading', (loading: boolean) => {
              if (loading === false) {
                  unwatch()
                  return response()
              }
          })
      })
    }

    async getRequest (endpoint: string) {
      await this.waitForAuthLoading()
      const token = await this.token
      console.log(token)
      if (token) {
        return await fetch(this.baseUrl + endpoint, {
          method: 'GET',
          mode: 'cors',
          headers: {
            Authorization: 'Bearer ' + token,
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': 'true'
          }
        })
      }
    }

    async postRequest (endpoint: string) {
      await this.waitForAuthLoading()
      const token = await this.token
      console.log(token)
      if (token) {
        return await fetch(this.baseUrl + endpoint, {
          method: 'POST',
          mode: 'cors',
          headers: {
            Authorization: 'Bearer ' + token,
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': 'true'
          }
        })
      }
    }

    async deleteRequest (endpoint: string) {
      await this.waitForAuthLoading()
      const token = await this.token
      console.log(token)
      if (token) {
        return await fetch(this.baseUrl + endpoint, {
          method: 'DELETE',
          mode: 'cors',
          headers: {
            Authorization: 'Bearer ' + token,
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': 'true'
          }
        })
      }
    }
}
