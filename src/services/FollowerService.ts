import { Vue } from 'vue-property-decorator'

export default class FollowerService extends Vue {
  async getFollowers (userName: string) {
    const response = await this.$apiService.getRequest('follower/?userName=' + userName)
    const responseJson = await response.json()
    return responseJson
  }

  async getFollowing (userName: string) {
    const response = await this.$apiService.getRequest('follower/following?userName=' + userName)
    const responseJson = await response.json()
    return responseJson
  }
  // niet via follower direct maar vanuit profile

  async unfollow (userName: string, guidCurrentSignIn: string) {
    const encodedValue = encodeURIComponent(guidCurrentSignIn)
    const response = await this.$apiService.deleteRequest('follower/unfollow?userName=' + userName + '&guidCurrentSignIn=' + encodedValue)
    const responseJson = await response.json()
    return responseJson
  }

  async follow (userId: string, userName: string, guidCurrentSignIn: string) {
    const encodedValue = encodeURIComponent(guidCurrentSignIn)
    const response = await this.$apiService.postRequest('profile/follow?userId=' + userId + '&userName=' + userName + '&guidCurrentSignIn=' + encodedValue)
    const responseJson = await response.json()
    return responseJson
  }
}
