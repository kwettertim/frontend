import { Vue } from 'vue-property-decorator'

export default class TimelineService extends Vue {
      async getTimeline () {
        const response = await this.$apiService.getRequest('timeline')
        const responseJson = response.json()
        return responseJson
      }

      async getTimelinePersonalTweets (guid: string) {
        const response = await this.$apiService.getRequest('timeline?guid=' + guid)
        const responseJson = response.json()
        return responseJson
      }

      async getTimelineUserTweets (userName: string) {
        const response = await this.$apiService.getRequest('timeline/userprofile?userName=' + userName)
        const responseJson = response.json()
        return responseJson
      }
}
