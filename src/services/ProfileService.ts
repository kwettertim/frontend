import { Vue } from 'vue-property-decorator'

export default class ProfileService extends Vue {
  async getProfile (id: string) {
    const encodedValue = encodeURIComponent(id)
    const response = await this.$apiService.getRequest('profile/?guid=' + encodedValue)
    const responseJson = await response.json()
    return responseJson
  }

  async getProfileUser (userName: string, id: string) {
    const encodedValue = encodeURIComponent(id)
    const response = await this.$apiService.getRequest('profile/user?userName=' + userName + '&guidCurrentSignIn=' + encodedValue)
    const responseJson = await response.json()
    return responseJson
  }

  async getUsers () {
    const response = await this.$apiService.getRequest('profile/all')
    const responseJson = await response.json()
    return responseJson
  }
}
