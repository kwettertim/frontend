import { Vue } from 'vue-property-decorator'

export default class TweetService extends Vue {
  async postTweet (tweet: any) {
    // const encodedValue = encodeURIComponent(id)
    const text = '?text=' + tweet.text
    const userName = '&userName=' + tweet.username
    const userId = '&userId=' + tweet.userId
    const response = await this.$apiService.postRequest('tweet/' + text + userName + userId)
    const responseJson = await response.json()
    console.log(responseJson)
    return null
  }
}
