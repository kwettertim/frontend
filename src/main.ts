import Vue from 'vue'
import App from './App.vue'
import LeftNavBar from './components/LeftNavBar.vue'
import Trending from './components/Trending.vue'
import ProfileRightNavBar from './components/profile/ProfileRightNavBar.vue'
import User from './components/users/User.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import Tweet from './components/Tweet.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleDown, faDragon, faHeart, faSearch, faUser, faUsers, faRetweet, faComment } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { Auth0Plugin } from './auth'
import ApiService from './services/APIService'
// import { domain, clientId } from '../auth.config.json'

Vue.config.productionTip = false
library.add(faDragon)
library.add(faAngleDown)
library.add(faHeart)
library.add(faUser)
library.add(faUsers)
library.add(faSearch)
library.add(faRetweet)
library.add(faComment)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('left-nav-bar', LeftNavBar)
Vue.component('profile-right-nav-bar', ProfileRightNavBar)
Vue.component('user', User)
Vue.component('trending', Trending)
Vue.component('tweet', Tweet)
Vue.config.productionTip = false

const domain = 'kwetterdev.eu.auth0.com'
const clientId = 'jZOoQAxUtjziC9dg7i2ATyptaHgSKcFi'
const audience = 'https://GatewayKwetter.com'

Vue.use(Auth0Plugin, {
  domain,
  clientId,
  audience,
  onRedirectCallback: (appState) => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    )
  }
})

Vue.use(ServiceFunc(ApiService, '$apiService'))

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

function ServiceFunc<T> (type: { new(): T}, name: string): {install: (Vue: any, options: object) => void} {
  return {
    install: (Vue: any, options: object) => {
      Vue.prototype[name] = new
      type ()
    }
  }
}
