import { shallowMount, createLocalVue } from '@vue/test-utils'
import User from '@/components/users/User.vue'

const localVue = createLocalVue();
localVue.config.silent = true;
let wrapper;

describe('Navbar logged in', () => {
  beforeEach(() => {
      wrapper = shallowMount(User, {
          localVue,
          stubs: ['router-link']
      });
      wrapper.setData({
          user: [
            { name: 'Tim', username: '@timc', roles: 'Administrator, Moderator' },
            { name: 'James', username: '@jam', roles: 'Administrator, Moderator' },
            { name: 'Pops', username: '@pops', roles: 'Administrator' }
        ]
      });
  });

  it("Shows username", () => {
      expect(wrapper.find("#UserName").text()).toContain("@timc");
  })
});

describe('User.vue', () => {
  it('renders', () => {
    const wrapper = shallowMount(User, {})
    expect(wrapper.exists()).toBe(true)
  })
})
